import {StatusBar} from 'expo-status-bar';
import React, {SetStateAction, useEffect, useState} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';

import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import {Text, View, ViewStyle} from 'react-native'
import * as Location from 'expo-location';
import Nav from "./components/nav";
import {LocationAccuracy, LocationOptions} from 'expo-location';

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();
  const [location, setLocation] = useState({
    coords: {
      longitude: ""
    }
  });
  const [errorMsg, setErrorMsg] = useState('');

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
        return;
      }
      const config = {
        enableHighAccuracy: true,
        accuracy: LocationAccuracy.BestForNavigation,
        timeInterval: 1000,
        distanceInterval: 0
      }
      let location = await Location.getCurrentPositionAsync(config);
      console.log(location)
      setLocation(location as unknown as SetStateAction<any>);

      await Location.watchPositionAsync(config, (loc) => {
        console.log(loc.coords)
        setLocation(loc as unknown as SetStateAction<any>)
      })
    })();
  }, []);

  let text = 'Waiting..';
  if (errorMsg) {
    text = errorMsg;
  } else if (location) {
    text = JSON.stringify(location);
  }

  const pos = {
    latTop: 47.324733,
    latBot: 47.324743,
    lonLeft: 39.68419647216797,
    lonRight: 39.68419647216897
  }

  const one = (pos.lonRight - pos.lonLeft) / 100
  const velichina = parseFloat(location.coords.longitude) - pos.lonLeft
  const left = (velichina * one).toFixed(10) + '%'
  console.log(location.coords.longitude, (velichina * one).toFixed(10) + '%')
  const dot = {
    width: '1%',
    height: '1%',
    backgroundColor: 'black',
    left: location ? left : '30%',
    top: '20%',
    zIndex: 3
  }



  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <SafeAreaProvider>
        {/*<View style={style as ViewStyle} />*/}
        {/*<View style={dot} />*/}
        {/*<View style={{ justifyContent: 'center', alignItems: 'center', width: '100%', height: '100%', zIndex: 2 }}>*/}
        {/*  <Text>{text}</Text>*/}
        {/*</View>*/}
        <Nav />

        {/*<Navigation colorScheme={colorScheme} />*/}
        <StatusBar />
      </SafeAreaProvider>
    );
  }
}



const style = {
  backgroundColor: 'red',
  minWidth: '100%',
  minHeight: '100%',
  zIndex: 1,
  position: 'absolute',
  backgroundImage: 'url(https://f.otzyv.ru/f/10/09/61710/28228/1508141335380.jpg)'
}
