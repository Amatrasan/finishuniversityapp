import { GLView } from "expo-gl";
import React from "react";
import Expo2DContext from "expo-2d-context";

const Nav = () => {

  const _onGLContextCreate = (gl) => {
    const ctx = new Expo2DContext(gl)
    ctx.translate(50, 100);
    // ctx.scale(4, 4);
    ctx.fillStyle = "grey";
    ctx.fillRect(20, 40, 100, 100);
    ctx.fillStyle = "white";
    ctx.fillRect(30, 100, 20, 30);
    ctx.fillRect(60, 100, 20, 30);
    ctx.fillRect(90, 100, 20, 30);
    ctx.beginPath();
    ctx.arc(50, 70, 18, 0, 2 * Math.PI);
    ctx.arc(90, 70, 18, 0, 2 * Math.PI);
    ctx.fill();
    ctx.fillStyle = "red";
    ctx.beginPath();
    ctx.arc(50, 70, 8, 0, 2 * Math.PI);
    ctx.arc(90, 70, 8, 0, 2 * Math.PI);
    ctx.fill();
    // ctx.strokeStyle = "red";
    // ctx.beginPath();
    // ctx.moveTo(70, 40);
    // ctx.lineTo(70, 30);
    // ctx.arc(70, 20, 10, 0.5 * Math.PI, 2.5 * Math.PI);
    ctx.stroke();
    ctx.flush();
  };

  return (
    <GLView style={{ flex: 1, width: 50, height: 50 }} onContextCreate={_onGLContextCreate} />
  );
}

export default Nav
